<?php

$serv = new Swoole\Server('127.0.0.1', 6666, SWOOLE_BASE);

$serv->set([
    'worker_num' => 1
]);

$serv->on('connect', function ($serv, $fd) {
    var_dump('Client:Connect ' . $fd);
});
$serv->on('receive', function ($serv, $fd, $reactor_id, $data) {
    var_dump(strlen($data));
    sleep(100000);
});
$serv->on('close', function ($serv, $fd) {
    var_dump('Close:' . $fd);
});
$serv->start();
