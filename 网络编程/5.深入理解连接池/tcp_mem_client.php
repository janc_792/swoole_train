<?php

$socket = new Co\Socket(AF_INET, SOCK_STREAM, 0);

go(function () use ($socket) {
    $socket->connect('127.0.0.1', 6666);
    $socket->setOption(SOL_SOCKET, SO_LINGER, ['l_onoff' => 1, 'l_linger' => 0]);
    $i = 0;
    while ($i++ < 10000) {
        var_dump($socket->send(str_repeat('1',1024)));
    }
    echo "done\n";
});