 ## 四次挥手 ##
 
  1. TCP客户端发送一个FIN报文，用来关闭客户到服务器的数据传送。
  2. 服务器收到这个FIN报文，它发回一个ACK报文，确认序号为收到的序号加1。和SYN一样，一个FIN报文将占用一个序号。
  3. 服务器关闭客户端的连接，发送一个FIN给客户端。
  4. 客户端发回ACK报文确认，并将确认序号设置为收到序号加1。
  
 - 四元组
 
 > client_ip client_port - server_ip server_port

## time_wait 问题##

- 持续一分钟

> 服务端未收到最后一步 ack 报文，会定时重传 第三次的 fin 
> 就需要客户端 time_wait 等待

> 数据包驻留，也需要 time_wait 状态等待，一分钟是 IP 包的存活时间

- Cannot assign requested address

> 端口数量用完了，根本原因是 time_wait 状态占用

```php
$redis = new Redis();

while (true) {
    var_dump($redis->connect('127.0.0.1'));
    // 连接完成就关闭，产生 time_wait 状态， 用光端口
    $redis->close();
}
```

- Address already in use(SO_REUSEADDR)

> 服务端口被 time_wait 占用了
>
> net.ipv4.tcp_timestamps=1;
>
> net.ipv4.tcp_tw_reuse=1;
>
> net.ipv4.ip_local_port_range最大;
>
> 不要开启 net.ipv4.tcp_tw_recycle=1.

## close_wait 问题 ##

- 服务端 onClose 不能顺利结束（阻塞、throw、die）

> SWOOLE_BASE 模式下不会出现这个问题，会自动结束
> SWOOLE_PROCESS 连接和主进程建立，会出现 close_wait 
