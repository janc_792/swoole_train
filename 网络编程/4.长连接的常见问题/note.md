## 连接失效 ##

- redis:timeout(Error while reading line from the server)
- mysql:wait_timeout & interactive_timeout(has gone away)

- 解决方法

(1) 用时重试
> 1. server 断开后占用连接资源（内存、端口、句柄）,被动断开 close wait 永远不会消失
>
> 2. 面临短连接的问题
>
> 3. 优点：简单


(2) 定时心跳维持长连接

### 维持长连接, TCP 的 keepalive ###

- keepalive
```php
$serv->set([
    'open_tcp_keepalive' => 1,// 开关
    'tcp_keepidle' => 4,// 4s 没有数据传输就进行检测
    'tcp_keepalive' => 1,// 1s 探测一次
    'tcp_keepcount' => 5,// 探测的次数，超过 5 次 close 此连接
]);
```
- heartbeat

> 利用 swoole 底层的 heartbeat_idle_time 和 heartbeat_check_interval   (检测死连)
>
> 只能针对服务端，需要客户端配合发送心跳 
>
> 无法解决假死问题，通过配置的方式不够灵活

## 应用层心跳 ##

- 制定 ping/pong 协议 (mysql 等自带 Ping 协议)
- 客户端灵活地发送 ping 心跳包
- 服务端 OnReceive 检查可用性回复 pong

```php
$serv->on('receive', function ($serv, $fd, $reactor_id, $data) {
    if ($data == 'ping') {
        checkDb();
        checkServiceA();
        checkRedis();
        $serv->send('pong');
    }
    var_dump($data);
});
```

## 结论 ##

- tcp 的 keepalive 最简单，但是有兼容性问题，不够灵活
- swoole 提供的 keepalive 最实用，但是需要客户端配合，复杂度适中
- 应用层的 keepalive 最灵活但是最麻烦


