<?php

$serv = new \Swoole\Server('0.0.0.0', 6666, SWOOLE_PROCESS);
$serv->set([
    'worker_num' => 1,
//    'open_tcp_keepalive' => 1,// 开关
//    'tcp_keepidle' => 4,// 4s 没有数据传输就进行检测
//    'tcp_keepalive' => 1,// 1s 探测一次
//    'tcp_keepcount' => 5,// 探测的次数，超过 5 次 close 此连接
    'heartbeat_check_interval' => 1,// 1s 探测一次
    'heartbeat_idle_time' => 5, // 5s 未发送数据包就 close 连接

]);

$serv->on('connect', function ($serv, $fd) {
    var_dump('Client:connect' . $fd);
});
$serv->on('receive', function ($serv, $fd, $reactor_id, $data) {
    if ($data == 'ping') {
        checkDb();
        checkServiceA();
        checkRedis();
        $serv->send('pong');
    }
    var_dump($data);
});

$serv->on('close', function ($serv, $fd) {
    var_dump('close fd' . $fd);
});

$serv->start();