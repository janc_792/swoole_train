- TCP
> 传输控制协议（TCP，Transmission Control Protocol）是一种面向连接的、可靠的、基于字节流的传输层通信协议

- 三次握手

1. 建立连接时，客户端发送syn包（syn=x）到服务器，并进入SYN_SENT状态，等待服务器确认；SYN：同步序列编号（Synchronize Sequence Numbers）;
2. 服务器收到syn包，必须确认客户的SYN（ack=x+1），同时自己也发送一个SYN包（syn=y），即SYN+ACK包，此时服务器进入SYN_RECV状态；
3. 客户端收到服务器的SYN+ACK包，向服务器发送确认包ACK(ack=y+1），此包发送完毕，客户端和服务器进入ESTABLISHED（TCP连接成功）状态，完成三次握手。

- 常见问题
 
 1.拒绝连接
 
 2.Operation now in progress 
> 丢包、错误ip、backlog满了&阻塞&tcp_abort_on_overflow=0
> *backlog:* 服务端收到第三次 ACK 的包的时候，会把已经建立好的连接放到 backlog 的队列里面。
> backlog 撑满后服务端会随机丢弃连接

> *tcp_abort_on_overflow:* 内核参数 为 1 的时候，在 backlog 满了以后会给客户端反馈一个连接已重置的错误 *Connection reset by peer*

 3.min(maxconn,backlog))  ss -lt
 
- SYN_FLOOD
 
> 半连接队列 *syn queue*
长度由 *tcp_max_syn_backlog* 设置 

> 可以将 *tcp_synack_retries* (第二次握手给客户端发送的包 syn+ack) 重置次数设置为 0 来避免 syn 攻击

> syn_cookies 默认打开


***Reactor 线程***

- Reactor 线程是在 Master 进程中创建的线程
- 负责维护客户端 TCP 连接、处理网络 IO、处理协议、收发数据
- 不执行任何 PHP 代码
- 将 TCP 客户端发来的数据缓冲、拼接、拆分成完整的一个请求数据包

- 查看 backlog 
```bash
netstat -s | grep 'times the listen queue of a socket overflowed'

netstat -s | grep "drop"
```

