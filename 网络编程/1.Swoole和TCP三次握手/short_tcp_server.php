<?php
$serv = new Swoole\Server('127.0.0.1', 6666, SWOOLE_BASE);
$serv->set([
    'worker_num' => 2
]);

$serv->on('connect', function ($serv, $fd) {
    var_dump("Client:Connect.\n");
});

/**
 *
 * 接收到数据时回调此函数，发生在 worker 进程中。
 * $reactor_id TCP 连接所在的 Reactor 线程 ID
 */
$serv->on('receive', function ($serv, $fd, $reactor_id, $data) {
    var_dump($data);
});

$serv->on('close', function ($serv, $fd) {
    var_dump('close');
});
$serv->start();