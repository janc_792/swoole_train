## mode ##

- SWOOLE_PROCESS 是在 `master` 进程维持连接，多一层 `ipc` 通信开销，但在工作进程崩溃是，连接因为是在 `master` 进程中，所以连接不会被断开。适用于维护大量长连接场景。

- SWOOLE_BASE 是在每一个工作进程中自己维持连接，所以性能更好， `HTTP` 更加适用。
