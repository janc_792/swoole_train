<?php
$pid = pcntl_fork();
var_dump($pid);
if ($pid !== 0) {
    echo "I am a parent process" . PHP_EOL;
} else {
    var_dump(getmypid());
    echo 'I am a sub process' . PHP_EOL;
}