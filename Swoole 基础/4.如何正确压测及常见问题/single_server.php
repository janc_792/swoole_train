<?php

use Swoole\Http\Server;
use Swoole\Http\Request;
use Swoole\Http\Response;

$http = new Server('127.0.0.1', 9501, SWOOLE_BASE);
$http->set([
    'log_level' => SWOOLE_LOG_INFO,
    'worker_num' => swoole_cpu_num(),
]);
$http->on('request', function (Request $request, Response $response) {
    $response->end('Hello Swoole');
});
$http->start();