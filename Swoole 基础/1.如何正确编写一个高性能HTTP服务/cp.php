<?php

$process = new Swoole\Process(function (Swoole\Process $process) {// 子进程
    $server = new Swoole\Http\Server('127.0.0.1', 9501, SWOOLE_BASE);
    $server->set([
        'log_file' => '/www/wwwroot/swoole_train/log.log',
        'log_level' => SWOOLE_LOG_INFO,
        'worker_num' => swoole_cpu_num() * 2,
        'hook_flags' => SWOOLE_HOOK_ALL,
    ]);

    $server->on('workerStart', function () use ($process, $server) {
//        $server->pool = new RedisQueue();
        $server->pool = new RedisPool(32);
        $process->write('1');
    });
    $server->on('request', function (Swoole\Http\Request $request, Swoole\Http\Response $response) use ($server) {
        try {
//            $redis = new Redis;
//            $redis->connect('127.0.0.1', 6379);
            /* @var $redis Redis */
            $redis = $server->pool->get();
            $greeter = $redis->get('greeter');
            if (!$greeter) {
                throw new RedisException('get data failed');
            }
            $server->pool->put($redis);
            $response->end("<h1>{$greeter}</h1>");
        } catch (Throwable $throwable) {
            $response->status(500);
            $response->end();
        }
    });
    $server->start();
});
if ($process->start()) {
    register_shutdown_function(function () use ($process) {
        $process::kill($process->pid);
        $process::wait();
    });
    $process->read(1);
    System('ab -c 1000 -n 100000 -k http://127.0.0.1:9501/ 2>&1');
}

class RedisQueue
{
    protected $pool;

    public function __construct()
    {
        $this->pool = new SplQueue;
    }

    public function get(): Redis
    {
        if ($this->pool->isEmpty()) {
            $redis = new Redis();
            $redis->connect('127.0.0.1', 6379);
            return $redis;
        }
        return $this->pool->dequeue();
    }

    public function put(Redis $redis)
    {
        $this->pool->enqueue($redis);
    }

    public function close(): void
    {
        $this->pool = null;
    }
}

class RedisPool
{
    /** @var \Swoole\Coroutine\Channel */
    protected $pool;

    /**
     *
     * RedisPool constructor.
     * @param int $size max connections
     */
    public function __construct(int $size = 100)
    {
        $this->pool = new \Swoole\Coroutine\Channel($size);
        for ($i = 0; $i < $size; $i++) {
            while (true) {
                try {
                    $redis = new \Redis();
                    $redis->connect('127.0.0.1', 6379);
                    $this->put($redis);
                    break;
                } catch (Throwable $throwable) {
                    usleep(1 * 1000);
                    continue;
                }
            }
        }
    }

    public function get(): \Redis
    {
        return $this->pool->pop();
    }

    public function put(Redis $redis)
    {
        $this->pool->push($redis);
    }

    public function close(): void
    {
        $this->pool->close();
        $this->pool = null;
    }
}