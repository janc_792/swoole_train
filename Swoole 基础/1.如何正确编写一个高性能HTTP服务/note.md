## Process ##

Process 是 Swoole 提供的进程管理模块，用来替代 PHP 的 `pcntl`

- 可以方便的实现进程间通讯;
- 支持重定向标准输入和输出，在子进程内 `echo` 不会打印屏幕，而是写入管道，读键盘输入可以重定向为管道读取数据;
- 提供了 exec 接口，创建的进程可以执行其他程序，与原 `PHP` 父进程之间可以方便的通信;

> 在协程环境中无法使用 `Process` 模块，可以使用 `runtime hook`+`proc_open`实现。


## 压测工具 ab ##

```shell script
yum -y install httpd-tools
```

## 连接池 ##

- `splQueue` 实现

```php
class RedisQueue
{
    protected $pool;

    public function __construct()
    {
        $this->pool = new SplQueue;
    }

    public function get(): Redis
    {
        if ($this->pool->isEmpty()) {
            $redis = new Redis();
            $redis->connect('127.0.0.1', 6379);
            return $redis;
        }
        return $this->pool->dequeue();
    }

    public function put(Redis $redis)
    {
        $this->pool->enqueue($redis);
    }

    public function close(): void
    {
        $this->pool = null;
    }
}
```

- `channel` 实现

```php
class RedisPool
{
    /** @var \Swoole\Coroutine\Channel */
    protected $pool;

    /**
     *
     * RedisPool constructor.
     * @param int $size max connections
     */
    public function __construct(int $size = 100)
    {
        $this->pool = new \Swoole\Coroutine\Channel($size);
        for ($i = 0; $i < $size; $i++) {
            while (true) {
                try {
                    $redis = new \Redis();
                    $redis->connect('127.0.0.1', 6379);
                    $this->put($redis);
                    break;
                } catch (Throwable $throwable) {
                    usleep(1 * 1000);
                    continue;
                }
            }
        }
    }

    public function get(): \Redis
    {
        return $this->pool->pop();
    }

    public function put(Redis $redis)
    {
        $this->pool->push($redis);
    }

    public function close(): void
    {
        $this->pool->close();
        $this->pool = null;
    }
}
```